class ProductoNoEncontradoError < StandardError
  def initialize(msg="El producto no esta en el carrito")
    super
  end
end

class CantidadDeProductoError < StandardError
  def initialize(msg="La cantidad ingresada no es valida. La misma debe ser un número natural")
    super
  end
end


class Carrito
  def initialize
    @productos = []
  end

  attr_reader :productos

  def evaluar_que_hacer_si_no_hay un_producto, si_es_verdadero, si_no_es_verdadero
    if !tiene_producto? un_producto
      si_es_verdadero.call
    else
      si_no_es_verdadero.call
    end
  end

  def agregar_producto! un_producto
    agregar_producto = proc { @productos.push un_producto }
    agregar_unidad = proc { un_producto.agregar_unidad! }
    evaluar_que_hacer_si_no_hay un_producto, agregar_producto, agregar_unidad
  end

  def cuantas_unidades_hay_de? un_producto
    retorna_cero = proc { return 0 }
    retornar_cuantas_unidades = proc { un_producto.cantidad_de_unidades }
    evaluar_que_hacer_si_no_hay un_producto, retorna_cero, retornar_cuantas_unidades
  end

  def tiene_producto? un_producto
    @productos.include? un_producto
  end

  def borrar_producto! un_producto
    lanzar_excepcion = proc { raise ProductoNoEncontradoError.new }
    borrar_producto_o_unidad = proc { if un_producto.cantidad_de_unidades == 1
                                        @productos.delete un_producto
                                      else
                                        un_producto.borrar_unidad!
                                      end }
    evaluar_que_hacer_si_no_hay un_producto, lanzar_excepcion, borrar_producto_o_unidad
  end

  def establecer_cantidad un_producto, una_cant
    if una_cant <= 0
      raise CantidadDeProductoError.new
    end
    lanzar_excepcion = proc { raise ProductoNoEncontradoError.new }
    establecer_cantidad_de_unidades = proc { un_producto.cantidad_de_unidades = una_cant.to_int }
    evaluar_que_hacer_si_no_hay un_producto, lanzar_excepcion, establecer_cantidad_de_unidades
  end
end