class Producto
  def initialize
    @cantidad_de_unidades = 1
  end

  attr_accessor :cantidad_de_unidades

  def agregar_unidad!
    @cantidad_de_unidades+=1
  end

  def borrar_unidad!
    @cantidad_de_unidades = [@cantidad_de_unidades-1,1].max
  end
end