require 'rspec'
require 'producto'

describe 'producto tests' do

  it 'Un producto recien creado tiene 1 de stock prductos' do
    un_producto_recien_creado = Producto.new
    expect(un_producto_recien_creado.cantidad_de_unidades).to eq 1
  end

  it 'Al agregar producto, se aumenta la cantidad de unidades en uno' do
    un_producto = Producto.new
    3.times { un_producto.agregar_unidad!}
    expect(un_producto.cantidad_de_unidades).to eq 4
  end

  it "Al borrar unidad se disminute la cantidad de unidades del producto" do
    un_producto = Producto.new
    2.times { un_producto.agregar_unidad!}
    expect(un_producto.cantidad_de_unidades).to eq 3
    un_producto.borrar_unidad!
    expect(un_producto.cantidad_de_unidades).to eq 2
    un_producto.borrar_unidad!
    expect(un_producto.cantidad_de_unidades).to eq 1
  end

  it "Al borrar unidades no se puede bajar de 1" do
    un_producto = Producto.new
    expect(un_producto.cantidad_de_unidades).to eq 1
    un_producto.borrar_unidad!
    expect(un_producto.cantidad_de_unidades).to eq 1
    100.times { un_producto.borrar_unidad! }
    expect(un_producto.cantidad_de_unidades).to eq 1
  end
end