require 'rspec'
require 'carrito'
require 'producto'

describe 'carrito tests' do

  it 'Un carrito recien creado se inicializa con 0 productos' do
    un_carrito_recien_creado = Carrito.new
    expect(cuantas_productos_distintos_hay_en un_carrito_recien_creado).to eq 0
  end

  it 'A un carrito recien creado se le agrega un producto y este tiene un solo producto' do
    un_carrito_recien_creado = Carrito.new
    agregar_n_cantidad_de_productos_a un_carrito_recien_creado, 1
    expect(cuantas_productos_distintos_hay_en un_carrito_recien_creado).to eq 1
  end

  it 'El carrito va aumentando la cantidad de productos a medida que estos se le agregan' do
    un_carrito = Carrito.new
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 0
    agregar_n_cantidad_de_productos_a un_carrito, 1
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 1
    agregar_n_cantidad_de_productos_a un_carrito, 2
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 3
    agregar_n_cantidad_de_productos_a un_carrito, 3
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 6
  end

  it 'Un carrito recien creado devuelve una lista vacia de productos al pedirle sus productos' do
    un_carrito_recien_creado = Carrito.new
    expect(cuantas_productos_distintos_hay_en un_carrito_recien_creado).to eq 0
  end

  it 'Un carrito al cual se le agrega un solo producto retorna una lista con un solo producto y es el indicado ' do
    un_carrito = Carrito.new
    un_carrito.agregar_producto! Producto.new
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 1
  end

  it 'Los productos del carrito van aumentando a medida que estos se le agregan productos al mismo' do
    un_carrito = Carrito.new
    expect(un_carrito.productos.size).to eq 0
    agregar_n_cantidad_de_productos_a un_carrito, 1
    expect(un_carrito.productos.size).to eq 1
    agregar_n_cantidad_de_productos_a un_carrito, 2
    expect(un_carrito.productos.size).to eq 3
    agregar_n_cantidad_de_productos_a un_carrito, 3
    expect(cuantas_productos_distintos_hay_en un_carrito).to eq 6
  end

  it "Al preguntarle cuantas unidades hay de un determinado  producto a un carrito vacio responde 0"do
    un_carrito_recien_creado = Carrito.new
    un_producto = Producto.new
    expect(un_carrito_recien_creado.cuantas_unidades_hay_de? un_producto).to eq 0
  end

  it "Al preguntarle cuantas unidades hay de un determinado  producto a un carrito que tiene una sola unidad de este retorna 1"do
    un_carrito_recien_creado = Carrito.new
    un_producto = Producto.new
    un_carrito_recien_creado.agregar_producto! un_producto
    expect(un_carrito_recien_creado.cuantas_unidades_hay_de? un_producto).to eq 1
  end

  it "Al preguntarle cuantas unidades hay de un determinado  producto a un carrito que tiene 4 unidades  de este retorna 4"do
    un_carrito_recien_creado = Carrito.new
    un_producto = Producto.new
    4.times {un_carrito_recien_creado.agregar_producto! un_producto }
    expect(un_carrito_recien_creado.cuantas_unidades_hay_de? un_producto).to eq 4
  end

  it "Al pedirle borrar un producto a un carrito vacio lanza una excepcion" do
    un_carrito_recien_creado = Carrito.new
    un_producto = Producto.new
    expect{ un_carrito_recien_creado.borrar_producto! un_producto }. to raise_error(ProductoNoEncontradoError)
  end


  it "Al pedirle borrar un producto a un carrito con productos lanza una excepcion cuando este no esta en él" do
    un_carrito_recien_creado = Carrito.new
    un_producto = Producto.new
    un_carrito_recien_creado.agregar_producto! Producto.new
    expect { un_carrito_recien_creado.borrar_producto! un_producto}.to raise_error(ProductoNoEncontradoError)
  end

  it "Al borrar un producto del carrito este desaparece cuando hay una sola unidad" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    expect(un_carrito.productos.include? un_producto).to be_falsey
    un_carrito.agregar_producto! un_producto
    expect(un_carrito.productos.include? un_producto).to be_truthy
    un_carrito.borrar_producto! un_producto
    expect(un_carrito.productos.include? un_producto).to be_falsey
  end

  it "Al borrar un producto del carrito este desaparece cuando hay muchas unidades del mismo producto unicamente" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    expect(un_carrito.productos.include? un_producto).to be_falsey
    4.times { un_carrito.agregar_producto! un_producto }
    expect(un_carrito.productos.include? un_producto).to be_truthy
    expect(cuantas_unidades_hay_en un_carrito).to eq 4
    un_carrito.borrar_producto! un_producto
    expect(cuantas_unidades_hay_en un_carrito).to eq 3
  end

  it "Al intentar establecer como cantidad de un producto 0 tira un error" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    un_carrito.agregar_producto! un_producto
    expect {un_carrito.establecer_cantidad un_producto, 0}.to raise_error(CantidadDeProductoError)
  end

  it "Al intenmtar establecer una cantidad no permitida de un producto tira un error" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    un_carrito.agregar_producto! un_producto
    expect {un_carrito.establecer_cantidad un_producto, -1}.to raise_error(CantidadDeProductoError)
  end


  it "Al intenmtar establecer una cantidad no permitida de un producto tira un error" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    expect {un_carrito.establecer_cantidad un_producto, -1}.to raise_error(CantidadDeProductoError)
  end

  it "Al intenmtar establecer una cantidad permitida de un producto este se modifica a dicha cantidad" do
    un_carrito = Carrito.new
    un_producto = Producto.new
    un_carrito.agregar_producto! un_producto
    un_carrito.establecer_cantidad un_producto, 42
    expect(un_producto.cantidad_de_unidades).to eq(42)
  end


  def cuantas_unidades_hay_en un_carrito
    un_carrito.productos.inject(0) { |total_unidades, un_producto| total_unidades + un_producto.cantidad_de_unidades }
  end

  def cuantas_productos_distintos_hay_en un_carrito
    un_carrito.productos.size
  end

  def agregar_n_cantidad_de_productos_a(un_carrito, una_cantidad)
    una_cantidad.times { un_carrito.agregar_producto! Producto.new }
  end
end