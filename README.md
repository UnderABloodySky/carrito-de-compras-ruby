# Carrito de compras en Ruby para Objetos 3 (UNQ) - 2c2020

- **Estudiante:** Horacio E. Valenzuela
- **Email:** [valenzuelahoracioe@gmail.com](valenzuelahoracioe@gmail.com)
- **Legajo:** 38613


##Ejercicio de introducción a Ruby 

---

Un supermercado de una ciudad de la región necesita un sistema para dar soporte a la realización de compras online, para así poder continuar brindando sus servicios evitando la concurrencia de personas a sus locales.
En particular, se desea implementar un carrito de compras, que permita realizar las siguientes operaciones:

- Agregar productos al carrito.
  - Si se agrega el mismo producto múltiples veces, habrá tantas unidades del producto en el carrito como veces que se agregó.
- Consultar qué productos tenemos agregados en el carrito.
- Consultar cuántas unidades de cierto producto tenemos en el carrito.
- Establecer la cantidad de unidades de un producto que hay en el carrito.
  - Por ejemplo, digo que ahora tengo 3 unidades de detergente en el carrito, independientemente de si haya tenido o no detergente antes, y de cuántas unidades haya tenido.
- Eliminar un producto del carrito.


Se pide:

    1. Modelar un objeto que represente al carrito, y que permita realizar las operaciones y consultas necesarias. 
       El programa debe incluir tests automatizados.
